"""
Тестовая программа для запуска приложения
по определению эмоциональной окраски рецензий
"""
#Кассы для работы приложения
from config.appConfig import AppConfig

from data.loadData import LoadData

from handlers.neuralNetwork.neuralNetworkCommandHandlerParameter import NeuralNetworkCommandHandlerParameter
from handlers.plotResult.plotResultCommandHandlerParameter import PlotResultCommandHandlerParameter
from handlers.neuralNetworkWordForecast.neuralNetworkWordForecastCommandHandlerParameter import NeuralNetworkWordForecastCommandHandlerParameter

from handlers.mainHandler import MainHandler
mainHandler= MainHandler()

appConfig = AppConfig()
appConfig.GetValue(appConfig.reviewsName)
loadData = LoadData(appConfig.GetValue(appConfig.reviewsName),
                   appConfig.GetValue(appConfig.ratingName))

if (appConfig.GetValue(appConfig.plName)[0]==1):

    vocab, inputDataset, targetDataset = loadData.mainLoad()

    appConfig.SetValue(appConfig.vocabName,
                           vocab)
    appConfig.SetValue(appConfig.inputDatasetName,
                           inputDataset)
    appConfig.SetValue(appConfig.targetDatasetName,
                       targetDataset)

    parameters = NeuralNetworkCommandHandlerParameter(appConfig.GetValue(appConfig.alphaName),
                                                    appConfig.GetValue(appConfig.iterationsName),
                                                    appConfig.GetValue(appConfig.hiddenSizeName),
                                                    appConfig.GetValue(appConfig.nTestName),
                                                    appConfig.GetValue(appConfig.vocabName),
                                                    appConfig.GetValue(appConfig.inputDatasetName),
                                                    appConfig.GetValue(appConfig.targetDatasetName),
                                                    appConfig.GetValue(appConfig.accuracyTrainName),
                                                    appConfig.GetValue(appConfig.accuracyTestName))

    accuracyTrain, accuracyTest = mainHandler.initFunction(0, parameters)

    appConfig.SetValue(appConfig.accuracyTrainName,
                           accuracyTrain)
    appConfig.SetValue(appConfig.accuracyTestName,
                       accuracyTest)

    parameters = PlotResultCommandHandlerParameter(appConfig.GetValue(appConfig.iterationsName),
                                                 appConfig.GetValue(appConfig.TrainName),
                                                    appConfig.GetValue(appConfig.TestName),
                                                    appConfig.GetValue(appConfig.accuracyTrainName),
                                                    appConfig.GetValue(appConfig.accuracyTestName))


    mainHandler.initFunction(1, parameters)

if (appConfig.GetValue(appConfig.plName)[1]==1):
    vocab2, concatenated, inputDataset2, word2index = loadData.concatenatedLoad()

    appConfig.SetValue(appConfig.vocabName2,
                           vocab2)
    appConfig.SetValue(appConfig.inputDatasetName2,
                           inputDataset2)
    appConfig.SetValue(appConfig.concatenatedName,
                       concatenated)
    appConfig.SetValue(appConfig.word2indexName,
                       word2index)

    parameters = NeuralNetworkWordForecastCommandHandlerParameter(appConfig.GetValue(appConfig.alphaName2),
                                                      appConfig.GetValue(appConfig.iterationsName2),
                                                      appConfig.GetValue(appConfig.hiddenSizeName2),
                                                      appConfig.GetValue(appConfig.windowName),
                                                      appConfig.GetValue(appConfig.negativeName),
                                                      appConfig.GetValue(appConfig.vocabName2),
                                                      appConfig.GetValue(appConfig.inputDatasetName2),
                                                      appConfig.GetValue(appConfig.concatenatedName),
                                                      appConfig.GetValue(appConfig.word2indexName))

    result = mainHandler.initFunction(2, parameters)


