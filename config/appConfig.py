class AppConfig():
    reviewsName = 'reviews'  # Название файла отзывов
    ratingName = 'rating'  # Название файла рейтинговой оценки
    alphaName = 'alpha' #Поправка перед обучением
    iterationsName = 'iterations' #Количество эпох обучения
    hiddenSizeName = 'hiddenSize' # Нейронов в скрытом слое
    nTestName = 'nTest' # Тестовая выборка
    vocabName = 'vocab' #Словарь уникальных слов
    inputDatasetName = 'inputDataset' #Входной датасет
    targetDatasetName = 'targetDataset' #Обучающие метки
    accuracyTrainName = 'accuracyTrain' #Точность обучения
    accuracyTestName = 'accuracyTest' #Точность тестирования
    TrainName = 'Trainstr'
    TestName = 'Teststr'
    plName='pl' #Выполнять или нет код
    #Параметры для подстановки нужных слов в фразы
    vocabName2 = 'vocab2'  # Словарь уникальных слов
    alphaName2 = 'alpha2'  # Поправка перед обучением
    iterationsName2 = 'iterations2'  # Количество эпох обучения
    hiddenSizeName2 = 'hiddenSize2'  # Нейронов в скрытом слое
    windowName = 'window'  #
    negativeName = 'negative'  #
    concatenatedName = 'concatenated'  #
    inputDatasetName2 = 'inputDataset2'
    word2indexName = 'word2index'
    values = {}

    def __init__(self):
        self.initEmptySettings()

    def initEmptySettings(self):
        # Создание всех имен переменных
        self.values[self.reviewsName] = 'reviews.txt'
        self.values[self.ratingName] = 'labels.txt'
        self.values[self.alphaName] = 0.01
        self.values[self.iterationsName] = 10
        self.values[self.hiddenSizeName] = 100
        self.values[self.nTestName] = 1000
        self.values[self.vocabName] = None
        self.values[self.inputDatasetName] = None
        self.values[self.targetDatasetName] = None
        self.values[self.accuracyTrainName] = [[0] for i in range(self.values[self.iterationsName])]
        self.values[self.accuracyTestName] = [[0]  for i in range(self.values[self.iterationsName])]
        self.values[self.plName] = [0, 1]

        self.values[self.vocabName2] = None
        self.values[self.alphaName2] = 0.05
        self.values[self.iterationsName2] = 10
        self.values[self.hiddenSizeName2] = 50
        self.values[self.windowName] = 2
        self.values[self.negativeName] = 5
        self.values[self.concatenatedName] = None
        self.values[self.inputDatasetName2] = None
        self.values[self.word2indexName] = None

        self.values[self.TrainName] = 'Точность при обучении'
        self.values[self.TestName] = 'Тестовая точность'

    def getNames(self):
        # Возвращает все названия параметров, необходимых для приложения
        keys = []
        for key, value in self.values.items():
            keys.append(key)
        return keys

    def GetValue(self, name):
        # Метод для получения значения параметра
        value = self.values[name]
        return value

    def SetValue(self, name, value):
        # Метод для изменения значения параметра
        for key, v in self.values.items():
            if name == key:
                self.values[name] = value