import numpy as np
import sys
import matplotlib.pyplot as plt

from handlers.baseCommandHandler import BaseCommandHandler

class PlotResultCommandHandler(BaseCommandHandler):
    def __init__(self):
        pass

    def execute(self, parameters):
        PlotResultCommandHandler.plot_accuracy(parameters.iterations, parameters.accuracyTrain, parameters.nameTrain)
        PlotResultCommandHandler.plot_accuracy(parameters.iterations, parameters.accuracyTest, parameters.nameTest)

    @staticmethod
    def plot_accuracy(epoch, acc, name):
        # Метод для графической визуализации точности по эпохам обучения
        fig = plt.figure(figsize=(15, 7))
        ax = [None] * 1  # len(acc)
        arrayEpoch = [i for i in range(epoch)]
        for iter in range(1):
            # y = []
            # for errorVector in acc:
            #    y.append(errorVector[iter])
            # только одна панель для рисования графиков
            ax[iter] = fig.add_subplot(1, 1, iter + 1)
            ax[iter].plot(arrayEpoch, acc, 'k')
            # график косинуса:
            # подпись по оси x
            ax[iter].set_xlabel('Эпоха')
            # подпись по оси y
            ax[iter].set_ylabel('Точность')
            # заголовок рисунка
            ax[iter].set_title(str(iter))
            ax[iter].grid()
        fig.show()
        fig.savefig(name + ".png", orientation='landscape', dpi=300)