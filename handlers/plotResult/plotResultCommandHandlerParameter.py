from handlers.baseCommandHandlerParameter import BaseCommandHandlerParameter

class PlotResultCommandHandlerParameter(BaseCommandHandlerParameter):
    def __init__(self, iterations, nameTrain, nameTest, accuracyTrain, accuracyTest):
        self.iterations = iterations
        self.nameTrain = nameTrain
        self.nameTest = nameTest
        self.accuracyTrain = accuracyTrain
        self.accuracyTest = accuracyTest