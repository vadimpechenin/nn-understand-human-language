"""
Описывает базовый класс для выполнения всех операций с данными
"""

from handlers.neuralNetwork.neuralNetworkCommandHandler import NeuralNetworkCommandHandler
from handlers.plotResult.plotResultCommandHandler import PlotResultCommandHandler
from handlers.neuralNetworkWordForecast.neuralNetworkWordForecastCommandHandler import NeuralNetworkWordForecastCommandHandler

class MainHandler():
    def __init__(self):
        self.dict = {}
        self.dict[0] = NeuralNetworkCommandHandler()
        self.dict[1] = PlotResultCommandHandler()
        self.dict[2] = NeuralNetworkWordForecastCommandHandler()


    def initFunction(self,code_request, parameter):
        result = None
        if code_request in self.dict:
            handler = self.dict[code_request]
            result = handler.execute(parameter)

        return result