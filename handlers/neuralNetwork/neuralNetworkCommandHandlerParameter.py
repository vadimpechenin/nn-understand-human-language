from handlers.baseCommandHandlerParameter import BaseCommandHandlerParameter

class NeuralNetworkCommandHandlerParameter(BaseCommandHandlerParameter):
    def __init__(self, alpha, iterations, hiddenSize, nTest, vocab, inputDataset, targetDataset, accuracyTrain, accuracyTest):
        self.alpha = alpha
        self.iterations = iterations
        self.hiddenSize = hiddenSize
        self.nTest = nTest
        self.vocab = vocab
        self.inputDataset = inputDataset
        self.targetDataset = targetDataset
        self.accuracyTrain = accuracyTrain
        self.accuracyTest = accuracyTest
