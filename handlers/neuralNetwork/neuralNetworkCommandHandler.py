import numpy as np
import sys

from handlers.baseCommandHandler import BaseCommandHandler
class NeuralNetworkCommandHandler(BaseCommandHandler):
    def __init__(self):
        pass

    def execute(self, parameters):
        #Основной метод для обучения нейронной сети
        np.random.seed(1)

        weights_0_1 = 0.2 * np.random.random((len(parameters.vocab), parameters.hiddenSize))-0.1
        weights_1_2 = 0.2 * np.random.random((parameters.hiddenSize,1)) - 0.1

        correct, total = (0, 0)
        for iter in range(parameters.iterations):

            for i in range(len(parameters.inputDataset)-parameters.nTest):
                x, y = (parameters.inputDataset[i], parameters.targetDataset[i])
                layer_1 = self.sigmoid(np.sum(weights_0_1[x], axis = 0)) #Векторное представление + sigmoid
                layer_2 = self.sigmoid(np.dot(layer_1, weights_1_2)) #Линейный слой + softmax

                layer_2_delta = layer_2 - y #Разность между прогнозом и истиной
                layer_1_delta = layer_2_delta.dot(weights_1_2.T) #Обратное распространение

                weights_0_1[x] -= layer_1_delta * parameters.alpha
                weights_1_2 -= np.outer(layer_1, layer_2_delta) * parameters.alpha

                if (np.abs(layer_2_delta) < 0.5):
                    correct += 1
                total += 1

                if (i%10 == 9):
                    progress = str(i/float(len(parameters.inputDataset)))
                    sys.stdout.write("\rИтерация:" + str(iter)\
                         +' Прогресс:' + progress[2:4]\
                         + '.' + progress[4:6]\
                         + '% Точность обучения: '\
                                    + str(correct/float(total)) + '%')

            parameters.accuracyTrain[iter]= correct/float(total)

            correct, total = (0, 0)
            for i in range(len(parameters.inputDataset)-parameters.nTest, len(parameters.inputDataset)):

                x = parameters.inputDataset[i]
                y = parameters.targetDataset[i]

                layer_1 = self.sigmoid(np.sum(weights_0_1[x], axis=0))
                layer_2 = self.sigmoid(np.dot(layer_1, weights_1_2))

                if (np.abs(layer_2 - y) < 0.5):
                    correct += 1
                total += 1
            print('Точность тестирования: ' + str(correct / float(total)))

            parameters.accuracyTest[iter] = correct / float(total)

        return parameters.accuracyTrain, parameters.accuracyTest
    def sigmoid(self,x):
        return 1/(1+np.exp(-x))



