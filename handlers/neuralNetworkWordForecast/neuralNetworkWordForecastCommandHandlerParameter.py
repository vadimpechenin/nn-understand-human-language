from handlers.baseCommandHandlerParameter import BaseCommandHandlerParameter

class NeuralNetworkWordForecastCommandHandlerParameter(BaseCommandHandlerParameter):
    def __init__(self, alpha, iterations, hiddenSize, window, negative, vocab, inputDataset, concatenated, word2index):
        self.alpha = alpha
        self.iterations = iterations
        self.hiddenSize = hiddenSize
        self.window = window
        self.negative = negative
        self.vocab = vocab
        self.concatenated = concatenated
        self.inputDataset = inputDataset
        self.word2index = word2index