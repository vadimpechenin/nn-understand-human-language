import numpy as np
import sys,math
from collections import Counter

from handlers.baseCommandHandler import BaseCommandHandler
class NeuralNetworkWordForecastCommandHandler(BaseCommandHandler):
    def __init__(self):
        pass

    def execute(self, parameters):
        #Метод для обучения нейронной сети для подстановки недостающих слов
        np.random.seed(1)

        weights_0_1 = 0.2 * (np.random.random((len(parameters.vocab), parameters.hiddenSize)) - 0.5)
        weights_1_2 = 0 * np.random.random((len(parameters.vocab),parameters.hiddenSize))

        layer_2_target = np.zeros(parameters.negative + 1)
        layer_2_target[0] = 1

        for rev_i, review in enumerate(parameters.inputDataset * parameters.iterations):
            for target_i in range(len(review)):
                # так как предугадывать каждый словарный запас очень трудоемко
                # мы собираемся предсказать только случайное подмножество
                target_samples = [review[target_i]] + list(parameters.concatenated \
                                                               [(np.random.rand(parameters.negative) * len(parameters.concatenated)).astype('int').tolist()])

                left_context = review[max(0, target_i - parameters.window):target_i]
                right_context = review[target_i + 1:min(len(review), target_i + parameters.window)]

                layer_1 = np.mean(weights_0_1[left_context + right_context], axis=0)
                layer_2 = self.sigmoid(layer_1.dot(weights_1_2[target_samples].T))
                layer_2_delta = layer_2 - layer_2_target
                layer_1_delta = layer_2_delta.dot(weights_1_2[target_samples])

                weights_0_1[left_context + right_context] -= layer_1_delta * parameters.alpha
                weights_1_2[target_samples] -= np.outer(layer_2_delta, layer_1) * parameters.alpha

            if (rev_i % 250 == 0):
                sys.stdout.write('\rProgress:' + str(rev_i / float(len(parameters.inputDataset)
                                                                   * parameters.iterations)) + "   " +
                                 str(self.similar(parameters.word2index, weights_0_1, 'terrible')))
            sys.stdout.write('\rProgress:' + str(rev_i / float(len(parameters.inputDataset)
                                                               * parameters.iterations)))
        print(self.similar(parameters.word2index, weights_0_1,'terrible'))

        return float(len(parameters.inputDataset) * parameters.iterations)

    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def similar(self, word2index,weights_0_1, target='beautiful'):
        target_index = word2index[target]

        scores = Counter()
        for word, index in word2index.items():
            raw_difference = weights_0_1[index] - (weights_0_1[target_index])
            squared_difference = raw_difference * raw_difference
            scores[word] = -math.sqrt(sum(squared_difference))
        return scores.most_common(10)