import sys,random
from collections import Counter
import numpy as np
import os

class LoadData():
    #Класс для работы с данными из текстовых файлов
    def __init__(self,reviewsName, labelsName):
        self.reviewsName = reviewsName
        self.labelsName = labelsName

    def openFile(self,fileName):
        #txtFilepath = os.path.join('txt', fileName)
        txtFilepath = "D:\\PYTHON\\Programms\\GrokkingDeepLearning\\NNUnderstandHumanLanguage\\data\\txt\\"
        f = open(txtFilepath+fileName)
        fileСontents = f.readlines()
        f.close()
        return fileСontents

    def mainLoad(self):
        #Основной метод предварительно подготовки слов
        #Метод для загрузки отзывов и рейтингов, создания векторного представления слов
        rawReviews = self.openFile(self.reviewsName)
        rawLabels  = self.openFile(self.labelsName)

        tokens = list(map(lambda x:set(x.split(" ")),rawReviews))

        vocab = set()
        for sent in tokens:
            for word in sent:
                if(len(word)>0):
                    vocab.add(word)
        vocab = list(vocab)

        word2index = {}
        for i,word in enumerate(vocab):
            word2index[word]=i

        inputDataset = list()
        for sent in tokens:
            sentIndices = list()
            for word in sent:
                try:
                    sentIndices.append(word2index[word])
                except:
                    ""
            inputDataset.append(list(set(sentIndices)))

        targetDataset = list()
        for label in rawLabels:
            if label == 'positive\n':
                targetDataset.append(1)
            else:
                targetDataset.append(0)

        return vocab, inputDataset, targetDataset

    def concatenatedLoad(self):
        #Метод предварительно подготовки слов для подстановки пропущенных
        #Метод для загрузки отзывов и рейтингов, создания векторного представления слов
        rawReviews = self.openFile(self.reviewsName)

        np.random.seed(1)
        random.seed(1)

        tokens = list(map(lambda x: (x.split(" ")), rawReviews))
        wordcnt = Counter()
        for sent in tokens:
            for word in sent:
                wordcnt[word] -= 1
        vocab = list(set(map(lambda x: x[0], wordcnt.most_common())))

        word2index = {}
        for i, word in enumerate(vocab):
            word2index[word] = i

        concatenated = list()
        inputDataset = list()
        for sent in tokens:
            sentIndices = list()
            for word in sent:
                try:
                    sentIndices.append(word2index[word])
                    concatenated.append(word2index[word])
                except:
                    ""
            inputDataset.append(sentIndices)
        concatenated = np.array(concatenated)
        random.shuffle(inputDataset)

        return vocab, concatenated, inputDataset, word2index